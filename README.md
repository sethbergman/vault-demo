# Demo of pulling secrets from Vault and using GitLab JWT for "token 0"

## Summary
The purpose of this project to to test the ability to pull secrets from vault and inject them into the pipeline.
GitLab has an [example in the documentation](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/) however it uses the [vault client](https://www.vaultproject.io/downloads) to authenticate and pull secrets. It requires manual loading of secrets into environment variables. 


[Hashicorp recomends](https://learn.hashicorp.com/vault/identity-access-management/vault-agent-aws#additional-discussion) the use of [envconsul](https://github.com/hashicorp/envconsul).

>  Furthermore, since Vault Agent Auto-Auth only addresses the challenges of obtaining and managing authentication tokens (secret zero), you might want to use helper tools such as Consul Template and Envconsul to obtain secrets stored in Vault (e.g. DB credentials, PKI certificates, AWS access keys, etc.).

## How This Project Works
GitLab as of `12.10` a [CI Predefined environment variable] is injected into each job in the form of an environment variable. We use this as "secret zero". We authenticate to Vault with `$CI_JOB_JWT` then load secrets into environment variables securely. 

The project contains a directory `.vault` with two files `prepare.sh` and `envconsul.hcl`. `prepare.sh` will fetch `envconsul`, request a token from Vault using the JWT, and create an alias `alias vaultrun="envconsul -once -config .vault/envconsul.hcl"`. `envconsul.hcl` contains the configuration and which secrets to request when executed. 

## What you will see
From Vault
```
$ vault kv get secret/infrastructure/demo
====== Metadata ======
Key              Value
---              -----
created_time     2020-05-08T17:12:18.660076805Z
deletion_time    n/a
destroyed        false
version          1

======= Data =======
Key            Value
---            -----
DEMO_DOMAIN    gitops-demo.com
DEMO_NAME      Brad Downey
```

From CI Output
```
source .vault/prepare.sh
envconsul_0.9.3_linux_amd64.tgz: OK
envconsul
$VAULT_TOKEN is NOT empty
vaultrun ./test.sh
Name Brad Downey Domain gitops-demo.com
```

## envconsul [![Build Status](https://circleci.com/gh/hashicorp/envconsul.svg?style=svg)](https://circleci.com/gh/hashicorp/envconsul)

Envconsul provides a convenient way to launch a subprocess with environment
variables populated from HashiCorp [Consul][consul] and [Vault][vault]. The tool
is inspired by [envdir][envdir] and [envchain][envchain], but works on many
major operating systems with no runtime requirements. It is also available
via a Docker container for scheduled environments.